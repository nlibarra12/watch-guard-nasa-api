import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class NasaService {
  
  url = "https://api.nasa.gov/mars-photos/api/v1/rovers/"
  
  constructor(private  http: HttpClient) { }

  getPhoto(rover?, dt?, camera?) {
    
    // API URL
    let api = "";
    
    if(camera === "all") {
      api = `${this.url}${rover}/photos?earth_date=${moment(dt).format('YYYY-MM-DD')}&api_key=JZpnhG8SqHxfohezDYUtEp8m4VATPnCtpmHTSHZC`;
    }else {
      api = `${this.url}${rover}/photos?earth_date=${moment(dt).format('YYYY-MM-DD')}&camera=${camera}&api_key=JZpnhG8SqHxfohezDYUtEp8m4VATPnCtpmHTSHZC`;
    }

    return this.http.get(api);
  }
}
