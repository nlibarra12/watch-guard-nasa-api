import { Component, OnInit } from '@angular/core';
import { NasaService } from '../../services/nasa.service';
import * as moment from 'moment';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit {

  toggle_call = false;
  toggle_email = false;


  photos: any = [];
  submitted = false;
  searching = false;
  submitted_error = false;
  submitted_error_text = "";


  filter_date: string = "2018-02-02";
  filter_rover: string = "curiosity";
  filter_camera: string = "all";

  import_file: any;

  constructor(private nasa: NasaService) { }

  ngOnInit() {
  }

  click_call(){
    this.toggle_call = !this.toggle_call;
  }

  click_email(){
    this.toggle_email = !this.toggle_email;
  }

  search(){

    this.searching = true;
    this.submitted = false;
    this.submitted_error = false;
    this.photos = [];

    // Validate date has been entered
    if (this.filter_date === undefined || this.filter_date === ""){
      
      this.searching = false;
      this.submitted = true;
      this.submitted_error = true;
      this.submitted_error_text = "Date is required";
      
      return;
    }

    this.nasa.getPhoto(this.filter_rover, this.filter_date, this.filter_camera ).subscribe((data:any) => {  
      
      // return data
      this.searching = false;
      this.submitted = true;
      this.photos = data.photos;

    }, error => {
      
      // Error, display
      this.searching = false;
      this.submitted = true;
      this.submitted_error = true;
      this.submitted_error_text = error.statusText;

    })
  }

  fileChanged(e) {
    this.import_file = e.target.files[0];
  }

  searchText(){
    this.searching = true;
    this.submitted = false;
    this.submitted_error = false;
    this.photos = [];

    // Loop through dates
    let fileReader = new FileReader();

    fileReader.onload = (e) => {

      var pattern = new RegExp('^((([1-2][0-9])|(3[0-1]))|([1-9]))/((1[0-2])|([1-9]))/[0-9]{4}$');
      var lines = fileReader.result.split('\n');

      for(var i = 0;i < lines.length;i++){

        if(!isNaN(Date.parse(lines[i]))){

          const dtLine = new Date(lines[i]);

          this.nasa.getPhoto("curiosity", dtLine, "all" ).subscribe((data:any) => {  
      
            // return data
            this.searching = false;
            this.submitted = true;

            // Push each element to array
            data.photos.forEach(element => {
              this.photos.push(element);
            });
      
          }, error => {
            
            // Error, display
            this.searching = false;
            this.submitted = true;
            this.submitted_error = true;
            this.submitted_error_text = error.statusText;

            return;
      
          })
        }     
      }
    }
    
    fileReader.readAsText(this.import_file);
 
  }

}
